/**
 *       @file  BackgroundLearningApp_exc.h
 *      @brief  The BackgroundLearningApp BarbequeRTRM application
 *
 * Description: Application "segment_object" of OpenCV examples
 *
 *     @author  Andrea d'Auria
 *				Elena Damiani
 				Giuseppe Massari
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2017
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#ifndef BACKGROUNDLEARNINGAPP_EXC_H_
#define BACKGROUNDLEARNINGAPP_EXC_H_

#include <bbque/bbque_exc.h>
#include <memory>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/video/background_segm.hpp>
#include <opencv2/videoio.hpp>

#include <time.h>

using namespace std;
using namespace cv;

using bbque::rtlib::BbqueEXC;

class BackgroundLearningApp : public BbqueEXC {


public:

	BackgroundLearningApp(std::string const & name,
			std::string const & recipe,
			RTLIB_Services_t *rtlib);

	virtual ~BackgroundLearningApp() { }

	/**
	 *	@brief Assign the value passed through command line to the class
	 *	attribute CamOrFile
	 */
	void setCamOrFile(string S) {
		CamOrFile.assign(S);
	}

	/**
	 * @brief Assign the value passed (through command line) in order to set an
	 * initial goal for the SetCPSGoal function
	 */
	void setQualityGoal(int i) {
		CPSGoal = i;
	}

	/**
	 * @brief Assigns the value passed (through command line) in order to set a
	 * value for TS that overwrites any quality preset
	 */
	void setThreshold(int i) {
		TS = i;
		if(i < -1) TS = -1;
	}

	/**
	 *	@brief Assign the value passed (through command line) in order to set
	 *	a value for niters that overwrites any quality preset
	 */
	void setIterations(int i) {
		niters = i;
		if(i < -1) niters = -1;
	}

	/**
	 *	@brief Check the compatibility of the parameter passed (through
	 *	command line) and do the correct assignment
	 */
	void setInternalQuality(int i) {
		quality = i;
		if(i < 0) quality = 0;
		if(i > 4) quality = 4;
	}

private:

	RTLIB_ExitCode_t onSetup();
	RTLIB_ExitCode_t onConfigure(int8_t awm_id);
	RTLIB_ExitCode_t onRun();
	RTLIB_ExitCode_t onMonitor();
	RTLIB_ExitCode_t onRelease();

	/**
	 *	@brief Produce contours more or less smoothed depending on the number
	 *	of iterations (niters) and generates the full areas
	 */
	void refineSegments(const Mat& img, Mat& mask, Mat& dst);

	/**
	 *	Define whether the application is in learning mode or not
	 */
	bool update_bg_model = true;

	VideoCapture cap;

	Mat tmp_frame;
	Mat bgmask;
	Mat out_frame;

	Ptr<BackgroundSubtractorMOG2> bgsubtractor =
		createBackgroundSubtractorMOG2();

	/**
	 *	Number of iterations used in dilate and erode functions
	 *	(the more iterations the less the quality of final image)
	 */
	int niters;

	/**
	 *	Number of cycles of the main loop in onRun section
	 *	Every cycle processes 1 frame
	 */
	int cycles;

	/**
	 *	Threshold is the tolerance of the difference between progressive pixels
	 *	The higher is the threshold, the hardest for the software to distinguish
	 *	different pixels consecutive to others
	 */
	int TS;

	int CPSGoal;
	
	/*** Keep track of changes in the goal during execution */
	int oldCPSGoal;	

	/**
	 *	Value utilised to do presetted combinations of TS and niters
	 *	(values accepted between 0 and 4)
	 */
	int quality;

	string CamOrFile;

	// Values of the input videofile
	double fps;
	double bright;
	double sat;
	double exp;
	double contrast;
	double width;
	double height;
	double hue;
};

#endif // BACKGROUNDLEARNINGAPP_EXC_H_
