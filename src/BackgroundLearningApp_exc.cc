/**
 *       @file  BackgroundLearningApp_exc.cc
 *      @brief  The BackgroundLearningApp BarbequeRTRM application
 *
 * Description: Application "segment_object" of OpenCV examples
 *
 *     @author  Andrea d'Auria
 *				Elena Damiani
 				Giuseppe Massari
 *
 *     Company  Your Company
 *   Copyright  Copyright (c) 2017
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#include "BackgroundLearningApp_exc.h"

#include <cstdio>
#include <bbque/utils/utility.h>


BackgroundLearningApp::BackgroundLearningApp(std::string const & name,
		std::string const & recipe,
		RTLIB_Services_t *rtlib) :
	BbqueEXC(name, recipe, rtlib) {

	logger->Notice("New BackgroundLearningApp::BackgroundLearningApp()");
	logger->Notice("EXC Unique IDentifier (UID): %u", GetUniqueID());
}


RTLIB_ExitCode_t BackgroundLearningApp::onSetup() {
	logger->Notice("BackgroundLearningApp::onSetup()");
	oldCPSGoal = CPSGoal;
	SetCPSGoal(CPSGoal - 1, CPSGoal + 1);

	cycles = 1;
	
	// Temporary Variables
	int niters_temp;
	int TS_temp;

	// Preset combinations of TS and niters
	// chosen according to the quality variable
	switch(quality) {
		case 0: {
			niters_temp = 0;
			TS_temp = 5;
			break;
		}

		case 1: {
			niters_temp = 1;
			TS_temp = 10;
			break;
		}

		case 2: {
			niters_temp = 1;
			TS_temp = 15;
			break;
		}

		case 3: {
			niters_temp = 3;
			TS_temp = 15;
			break;
		}

		case 4: {
			niters_temp = 5;
			TS_temp = 15;
			break;
		}
	}

	// If niter is -1 means no value has been specified through command line
	if(niters == -1) niters = niters_temp;

	// If TS is -1 means no value has been specified through command line
	if(TS == -1) TS = TS_temp;				

	// Setting the threshold
	bgsubtractor->setVarThreshold(TS);

	// Check whether to open internal webcam
	// or video file
	if(CamOrFile.compare("0") == 0) {
		cap.open(0);
		if(!cap.isOpened())
			logger->Error("Cannot open default camera");
	}
	else cap.open(CamOrFile);

	if(!cap.isOpened()) {
		return RTLIB_EXC_WORKLOAD_NONE;
	}

	cap >> tmp_frame;
	puts("");
	puts("Video Parameters:");

	fps      = cap.get(CAP_PROP_FPS);
	bright   = cap.get(CAP_PROP_BRIGHTNESS);
	sat      = cap.get(CAP_PROP_SATURATION);
	exp      = cap.get(CAP_PROP_AUTO_EXPOSURE);
	contrast = cap.get(CAP_PROP_CONTRAST);
	width    = cap.get(CAP_PROP_FRAME_WIDTH);
	height   = cap.get(CAP_PROP_FRAME_HEIGHT);
	hue      = cap.get(CAP_PROP_HUE);
	
	logger->Info("FPS: %.2f\nWIDTH: %.2f\nHEIGHT: %.2f\nSAT: %.2f\n"
			"BRIGHT: %.2f\nEXPOSURE: %.2f\nCONTRAST: %.2f\nHUE: %.2f\n\n",
			fps, width, height, sat, bright, exp, contrast, hue);

	return RTLIB_OK;
}


RTLIB_ExitCode_t BackgroundLearningApp::onConfigure(int8_t awm_id) {

	logger->Notice("BackgroundLearningApp::onConfigure(): EXC [%s] => AWM [%02d]",
		exc_name.c_str(), awm_id);

	return RTLIB_OK;
}


RTLIB_ExitCode_t BackgroundLearningApp::onRun() {
	// Every cycle of this loop processes 1 frame
	// The higher the resolution, the slower the onRun execution
	for(int i=0; i<cycles; i++) {
        cap >> tmp_frame;
        if( tmp_frame.empty() ) {
			logger->Warn("empty frame");
            break;
		}

	// Core Functions:
		// This creates the mask: it checks pixel similarity with the previous
		// frame and (while in learning mode) if it is beyond the threshold
		// it deletes it from the mask
		bgsubtractor->apply(tmp_frame, bgmask, update_bg_model ? -1 : 0);

		// This smooths contours and fill areas
		// The higher niters, the smoother the image and the easier
		// the video to be processed - therefore run time will be shorter
        refineSegments(tmp_frame, bgmask, out_frame);
		imshow("video", tmp_frame);
		imshow("segmented", out_frame);
	}

	char keycode = (char) waitKey(10);

	// Press ESC to terminate the application
	if( keycode == 27 ) {
		return RTLIB_EXC_WORKLOAD_NONE;
	}

	// Press space bar to switch from learning to non-learning mode and viceversa
	if( keycode == ' ' ) {
		update_bg_model = !update_bg_model;
		logger->Notice("Learn background is in state = %d\n",update_bg_model);
	}

	// Press "+" to increase CPSGoal
	if( keycode == '+' ) {
		++CPSGoal;
		logger->Notice("Increase frame-rate! => %d", CPSGoal);
	}

	// Press "-" to decrease CPSGoal
	if( keycode == '-' ) {
		--CPSGoal;
		logger->Notice("Decrease frame rate! => %d", CPSGoal);
	}

	logger->Debug("OCV keycode = %d", keycode);
	keycode = 0;

	return RTLIB_OK;
}


RTLIB_ExitCode_t BackgroundLearningApp::onMonitor() {

	logger->Notice("Cycle: %4d Goal: %d CPS = %.2f",
			Cycles(), CPSGoal, GetCPS());

	// Checking for changes in CPSGoal
	if(oldCPSGoal != CPSGoal) {
		SetCPSGoal(CPSGoal - 1, CPSGoal + 1);
		oldCPSGoal = CPSGoal;
		logger->Notice("Setting new CPS goal = %2.f", CPSGoal);
	}

	return RTLIB_OK;
}


RTLIB_ExitCode_t BackgroundLearningApp::onRelease() {

	logger->Info("BackgroundLearningApp::onRelease()  : exit");

	return RTLIB_OK;
}


void BackgroundLearningApp::refineSegments(const Mat& img, Mat& mask, Mat& dst) {
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	Mat temp;
	dilate(mask, temp, Mat(), Point(-1,-1), niters);
	erode(mask, temp, Mat(), Point(-1,-1), niters*2);
	dilate(temp, temp, Mat(), Point(-1,-1), niters);
	findContours( temp, contours, hierarchy, RETR_CCOMP, CHAIN_APPROX_SIMPLE );
	dst = Mat::zeros(img.size(), CV_8UC3);
	if( contours.size() == 0 )
		return;
	// iterate through all the top-level contours,
	// draw each connected component with its own random color
	int idx = 0, largestComp = 0;
	double maxArea = 0;
	for( ; idx >= 0; idx = hierarchy[idx][0] ) {
		const vector<Point>& c = contours[idx];
		double area = fabs(contourArea(Mat(c))); 
		if( area > maxArea ) {
			maxArea = area*1;
			largestComp = idx;
		}
	}

	// You can change colors by editing the RGB values
	Scalar color( 250, 100, 50 );
	drawContours( dst, contours, largestComp, color, FILLED, LINE_8, hierarchy );
}


