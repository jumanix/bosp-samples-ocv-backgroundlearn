/**
 *       @file  BackgroundLearningApp_main.cc
 *      @brief  The BackgroundLearningApp BarbequeRTRM application
 *
 * Description: Application "segment_object" of OpenCV examples
 *
 *     @author  Andrea d'Auria
 *				Elena Damiani
 *
 *     Company  Your Company
 *   Copyright  Copyright (c) 20XX, Name Surname
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#include <cstdio>
#include <iostream>
#include <random>
#include <cstring>
#include <memory>

#include <libgen.h>

#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#include "version.h"
#include "BackgroundLearningApp_exc.h"
#include <bbque/utils/utility.h>
#include <bbque/utils/logging/logger.h>


// Setup logging
#undef  BBQUE_LOG_MODULE
#define BBQUE_LOG_MODULE "BackgroundLearningApp"

namespace bu = bbque::utils;
namespace po = boost::program_options;

/**
 * @brief A pointer to an EXC
 */
std::unique_ptr<bu::Logger> logger;

/**
 * @brief A pointer to an EXC
 */
typedef std::shared_ptr<BbqueEXC> pBbqueEXC_t;

/**
 * The decription of each BackgroundLearningApp parameters
 */
po::options_description opts_desc("BackgroundLearningApp Configuration Options");

/**
 * The map of all BackgroundLearningApp parameters values
 */
po::variables_map opts_vm;

/**
 * The services exported by the RTLib
 */
RTLIB_Services_t *rtlib;

/**
 * @brief The application configuration file
 */
std::string conf_file = BBQUE_PATH_PREFIX "/" BBQUE_PATH_CONF "/BackgroundLearningApp.conf" ;

/**
 * @brief The recipe to use for all the EXCs
 */
std::string recipe;

/**
 * @brief The EXecution Context (EXC) registered
 */
std::shared_ptr<BackgroundLearningApp> pexc;

/**
*	The parameter utilised to set values for TS (threshold) and niters (number of iterations)
*/
int quality;
#define DEFAULT_QUALITY 1

/**
*	The parameter utilised to set a specific value for TS (threshold) that overwrites any preset
*/
int pixelThreshold;
#define DEFAULT_THRESHOLD -1

/**
*	The parameter utilised to set a specific value for niters (iterations) that overwrites any preset
*/
int iterations;
#define DEFAULT_ITERATIONS -1

/**
*	Cycles Per Second, utilised to set the first CPSGoal
*/
int setCPS;
#define DEFAULT_CPS 5

/**
*	Path of the file to open
*/
std::string chooseFile;


void ParseCommandLine(int argc, char *argv[]) {
	// Parse command line params
	try {
	po::store(po::parse_command_line(argc, argv, opts_desc), opts_vm);
	} catch(...) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_FAILURE);
	}
	po::notify(opts_vm);

	// Check for help request
	if (opts_vm.count("help")) {
		std::cout << "Usage: " << argv[0] << " [options]\n";
		std::cout << opts_desc << std::endl;
		::exit(EXIT_SUCCESS);
	}

	// Check for version request
	if (opts_vm.count("version")) {
		std::cout << "BackgroundLearningApp (ver. " << g_git_version << ")\n";
		std::cout << "Copyright (C) 2011 Politecnico di Milano\n";
		std::cout << "\n";
		std::cout << "Built on " <<
			__DATE__ << " " <<
			__TIME__ << "\n";
		std::cout << "\n";
		std::cout << "This is free software; see the source for "
			"copying conditions.  There is NO\n";
		std::cout << "warranty; not even for MERCHANTABILITY or "
			"FITNESS FOR A PARTICULAR PURPOSE.";
		std::cout << "\n" << std::endl;
		::exit(EXIT_SUCCESS);
	}
}

int main(int argc, char *argv[]) {

	opts_desc.add_options()
		("help,h", "\nHELP: \n"
            "This program demonstrated a simple method of connected components clean up of background subtraction\n"
            "When the program starts, it begins learning the background.\n"
            "You can toggle background learning on and off by hitting the space bar and change the CPSGoal by hitting + or -.\n\n")

		("version,v", "print program version\n")

		("conf,C", po::value<std::string>(&conf_file)->
			default_value(conf_file),
			"Ciao configuration file\n")

		("recipe,r", po::value<std::string>(&recipe)->
			default_value("BackgroundLearningApp"),
			"recipe name (for all EXCs)\n")

		("quality,q", po::value<int>(&quality)->
			default_value(DEFAULT_QUALITY),
			"Set a quality parameter between 0 and 4 in order to "
			"setup the threshold of pixel difference tolerance and "
			"number of iteration of dilate and erode in refineSegments function\n")

		("pathfile,p", po::value<std::string>(&chooseFile)->
			default_value("0"),
			"Path of the file you want to open. No input opens internal webcam\n")

		("cpsgoal,c", po::value<int>(&setCPS)->
			default_value(DEFAULT_CPS),
			"Set the number of Cycles Per Second\n")

		("threshold,t", po::value<int>(&pixelThreshold)->
			default_value(DEFAULT_THRESHOLD),
			"Set a specific value of the threshold of pixel difference tolerance "
			"that overwrites any quality preset\n")

		("iterations,i", po::value<int>(&iterations)->
			default_value(DEFAULT_ITERATIONS),
			"Set a specific value of the number of iterations "
			"that overwrites any quality preset\n")
	;

	// Setup a logger
	bu::Logger::SetConfigurationFile(conf_file);
	logger = bu::Logger::GetLogger("backgroundlearningapp");

	ParseCommandLine(argc, argv);

	// Welcome screen
	logger->Info(".:: BackgroundLearningApp (ver. %s) ::.", g_git_version);
	logger->Info("Built: " __DATE__  " " __TIME__);

	// Initializing the RTLib library and setup the communication channel
	// with the Barbeque RTRM
	logger->Info("STEP 0. Initializing RTLib, application [%s]...",
			::basename(argv[0]));

	if ( RTLIB_Init(::basename(argv[0]), &rtlib) != RTLIB_OK) {
		logger->Fatal("Unable to init RTLib (Did you start the BarbequeRTRM daemon?)");
		return RTLIB_ERROR;
	}

	assert(rtlib);

	logger->Info("STEP 1. Registering EXC using [%s] recipe...",
			recipe.c_str());
	pexc = std::make_shared<BackgroundLearningApp>("BackgroundLearningApp", recipe, rtlib);
	if (!pexc->isRegistered()) {
		logger->Fatal("Registering failure.");
		return RTLIB_ERROR;
	}

	// Setting the main variables needed to run the application
	pexc->setCamOrFile(chooseFile.c_str());
	pexc->setQualityGoal(setCPS);
	pexc->setInternalQuality(quality);
	pexc->setThreshold(pixelThreshold);
	pexc->setIterations(iterations);


	logger->Info("STEP 2. Starting EXC control thread...");
	pexc->Start();


	logger->Info("STEP 3. Waiting for EXC completion...");
	pexc->WaitCompletion();


	logger->Info("STEP 4. Disabling EXC...");
	pexc = NULL;

	logger->Info("===== BackgroundLearningApp DONE! =====");
	return EXIT_SUCCESS;

}

