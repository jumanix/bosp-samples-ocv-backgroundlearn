
ifdef CONFIG_SAMPLES_BACKGROUNDLEARNINGAPP

# Targets provided by this project
.PHONY: samples_opencv_backgroundlearningapp clean_samples_opencv_backgroundlearningapp

# Add this to the "samples_opencv_" targets
samples_opencv: samples_opencv_backgroundlearningapp
clean_samples_opencv: clean_samples_opencv_backgroundlearningapp

MODULE_SAMPLES_OCV_BACKGROUNDLEARNINGAPP=samples/opencv/background_learning

samples_opencv_backgroundlearningapp:
	@echo
	@echo "==== Building BackgroundLearningApp ($(BUILD_TYPE)) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(BOSP_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(MODULE_SAMPLES_OCV_BACKGROUNDLEARNINGAPP)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_SAMPLES_OCV_BACKGROUNDLEARNINGAPP)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_SAMPLES_OCV_BACKGROUNDLEARNINGAPP)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS="$(TARGET_FLAGS)" \
		CXX=$(CXX) CXXFLAGS="$(TARGET_FLAGS)" \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_SAMPLES_OCV_BACKGROUNDLEARNINGAPP)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_samples_opencv_backgroundlearningapp:
	@echo
	@echo "==== Clean-up BackgroundLearningApp Application ===="
	@rm -rf $(MODULE_SAMPLES_OCV_BACKGROUNDLEARNINGAPP)/build
	@echo

else # CONFIG_SAMPLES_BACKGROUNDLEARNINGAPP

samples_opencv_backgroundlearningapp:
	$(warning Background Learning application disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_SAMPLES_BACKGROUNDLEARNINGAPP

